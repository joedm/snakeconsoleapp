﻿using System;
using System.Collections.Generic;
using Snake.GameObjects;

namespace Snake.Extensions
{
    public static class ConsoleExtension
    {
        public static bool IsDirectionKey(this ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.A:
                case ConsoleKey.S:
                case ConsoleKey.D:
                case ConsoleKey.W:
                case ConsoleKey.UpArrow:
                case ConsoleKey.LeftArrow:
                case ConsoleKey.DownArrow:
                case ConsoleKey.RightArrow:
                    return true;
                default:
                    return false;
            }
        }

        public static IEnumerable<Direction> ToGameDirections(this ConsoleKey key)
        {
            if (key == ConsoleKey.W || key == ConsoleKey.UpArrow)
                yield return Direction.Up;

            if (key == ConsoleKey.S || key == ConsoleKey.DownArrow)
            {
                yield return Direction.Down;
            }

            if (key == ConsoleKey.D || key == ConsoleKey.RightArrow)
            {
                yield return Direction.Right;
            }

            if (key == ConsoleKey.A || key == ConsoleKey.LeftArrow)
            {
                yield return Direction.Left;
            }
        }

        public static ConsoleColor ToConsoleColour(this Colour myColour)
        {
            switch (myColour)
            {
                case Colour.Black:
                    return ConsoleColor.Black;
                case Colour.White:
                    return ConsoleColor.White;
                case Colour.Green:
                    return ConsoleColor.Green;
                case Colour.Red:
                    return ConsoleColor.Red;
                case Colour.Yellow:
                    return ConsoleColor.Yellow;
                case Colour.Gray:
                    return ConsoleColor.Gray;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}