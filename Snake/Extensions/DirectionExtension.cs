﻿using System.Collections;
using System.Collections.Generic;
using Snake.GameObjects;

namespace Snake.Extensions
{
    public static class DirectionExtension
    {
        public static IEnumerable<Direction> DistinctStrokes(this IEnumerable<Direction> directions)
        {
            Direction lastDirection = null;

            foreach (var direction in directions)
                if (direction != lastDirection)
                    yield return lastDirection = direction;
        }
    }
}