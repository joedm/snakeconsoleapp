﻿using System.Collections.Generic;
using System.Linq;

namespace Snake.GameObjects
{
    public class Snake
    {
        public List<CoOrdinate> Blocks { get; set; }
        public Direction DirectionOfTravel { get; set; }
        public int Length => Blocks.Count;
        public CoOrdinate CurrentHead => Blocks.First();
        public CoOrdinate CurrentTail => Blocks.Last();

        public Snake(int headXPosition, int headYPosition, int length = 5)
        {
            Blocks = new List<CoOrdinate>();
            DirectionOfTravel = Direction.Right;

            for (int i = 0; i < length; i++)
            {
                Blocks.Add(new CoOrdinate(headXPosition - i, headYPosition));
            }
        }
        
        public void Move()
        {
            int nextX = DirectionOfTravel == Direction.Up || DirectionOfTravel == Direction.Down ? CurrentHead.X :
                DirectionOfTravel == Direction.Left ? CurrentHead.X - 1
                : CurrentHead.X + 1;

            int nextY = DirectionOfTravel == Direction.Left || DirectionOfTravel == Direction.Right ? CurrentHead.Y :
                DirectionOfTravel == Direction.Up ? CurrentHead.Y - 1
                : CurrentHead.Y + 1;

            Blocks.Insert(0, new CoOrdinate(nextX, nextY));
            Blocks.RemoveAt(Blocks.Count - 1);
        }
    }
}