﻿using System;

namespace Snake.GameObjects
{
    public class Direction : IComparable<Direction>
    {
        public static Direction Up => new Direction(1);
        public static Direction Down => new Direction(-1);
        public static Direction Left => new Direction(2);
        public static Direction Right => new Direction(-2);

        public int Value { get; }

        private Direction(int direction)
        {
            Value = direction;
        }

        public Direction Invert()
        {
            return new Direction(Value * -1);
        }
        
        public static implicit operator int(Direction d)
        {
            return d.Value;
        }

        public static implicit operator Direction(int d)
        {
            return new Direction(d);
        }

        public static bool operator ==(Direction x, Direction y)
        {
            return x?.Value == y?.Value;
        }

        public static bool operator !=(Direction x, Direction y)
        {
            return x?.Value != y?.Value;
        }

        public override bool Equals(object obj)
        {
            var other = obj as Direction;
            return Value == other?.Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public int CompareTo(Direction other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Value.CompareTo(other.Value);
        }
    }
}