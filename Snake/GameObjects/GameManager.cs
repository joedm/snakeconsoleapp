﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Snake.Extensions;

// ReSharper disable SwitchStatementMissingSomeCases

namespace Snake.GameObjects
{
    public class GameManager
    {
        public Level Level { get; set; }
        public int BaseMoveInterval { get; set; }
        public long NextMove { get; set; }
        public Stopwatch Timer { get; set; }
        public int MoveInterval => (int)(Math.Exp(_moveIntervalSlope * Level.Score) * BaseMoveInterval);

        private double _moveIntervalSlope;

        public void Start(
            int gridHeight = 40,
            int gridWidth = 40,
            int snakeLength = 5,
            int score = 0,
            [Range(1, 10)]int difficulty = 4,
            [Range(1, 10)]int speed = 5
            )
        {
            Console.Clear();
            Level = new Level(gridHeight, gridWidth, snakeLength);

            if(score == int.MinValue)
                Level.FoodCollected = snakeLength - 5 * Level.PointsPerFood;
            else
                Level.FoodCollected = score / Level.PointsPerFood;

            BaseMoveInterval = (11 - speed) * 50;
            _moveIntervalSlope = -0.1 * difficulty / BaseMoveInterval;
            Timer = Stopwatch.StartNew();
            NextMove = MoveInterval;
            
            while (!Level.GameOver)
            {
                HandleDirectionKeyPress();

                if (Timer.ElapsedMilliseconds > NextMove)
                {
                    Move();
                }
            }

            Console.ReadLine();

            // Restart the level on game over.
            Start(gridHeight, gridWidth, snakeLength, score, difficulty);
        }

        private void HandleDirectionKeyPress()
        {
            var inputDirections = GetInputDirectionsForKeyPresses()
                .DistinctStrokes()
                .OrderByDescending(x => Level.Snake.DirectionOfTravel.Invert())
                .ThenBy(x => x)
                .ToList();

            foreach (var direction in inputDirections)
            {
                if (direction == Level.Snake.DirectionOfTravel.Invert())
                    continue;

                Level.Snake.DirectionOfTravel = direction;
                Move();
            }
        }

        public IEnumerable<Direction> GetInputDirectionsForKeyPresses()
        {
            while (Console.KeyAvailable)
            {
                var keys = Console.ReadKey(true).Key;
                if (!keys.IsDirectionKey())
                    continue;

                foreach (var key in keys.ToGameDirections())
                    yield return key;
            }
        }

        public void Move()
        {
            NextMove = Timer.ElapsedMilliseconds + MoveInterval;
            Level.Move();
            RenderGrid();
        }
        
        public void RenderGrid()
        {
            Console.SetWindowSize(Level.Width * 2 + 2, Level.Height + 3);
            Console.SetBufferSize(Level.Width * 2 + 2, Level.Height + 3);
            Console.CursorVisible = false;

            foreach (var row in Level.Grid)
            {
                foreach (var cell in row)
                {
                    if (!cell.HasChanges)
                        continue;

                    Console.SetCursorPosition(cell.CoOrdinate.X * 2 + 1, cell.CoOrdinate.Y + 2);
                    Console.BackgroundColor = cell.BackgroundColour.ToConsoleColour();
                    Console.ForegroundColor = cell.TextColour.ToConsoleColour();
                    Console.Write(cell.Characters);

                    cell.HasChanges = false;
                }
            }

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(1,0);
            Console.Write($"Moves: {Level.Moves}");

            string scoreText = $"Score: {Level.Score}";
            Console.SetCursorPosition(Level.Width * 2 - scoreText.Length - 1, 0);
            Console.Write(scoreText);
        }
    }
}