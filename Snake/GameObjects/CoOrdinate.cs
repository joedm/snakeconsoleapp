﻿namespace Snake.GameObjects
{
    public class CoOrdinate
    {
        public int X { get; set; }
        public int Y { get; set; }

        public CoOrdinate(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}