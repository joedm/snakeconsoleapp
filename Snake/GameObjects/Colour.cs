﻿namespace Snake.GameObjects
{
    public enum Colour
    {
        Black,
        White,
        Green,
        Red,
        Yellow,
        Gray
    }
}