﻿namespace Snake.GameObjects.Blocks
{
    public abstract class BaseBlock
    {
        public CoOrdinate CoOrdinate { get; set; }
        public string Characters { get; set; }
        public Colour TextColour { get; set; }
        public Colour BackgroundColour { get; set; }
        public bool HasChanges { get; set; } = true;

        protected BaseBlock(CoOrdinate coOrdinate, string characters, Colour textColour, Colour backgroundColour)
        {
            CoOrdinate = coOrdinate;
            Characters = characters;
            TextColour = textColour;
            BackgroundColour = backgroundColour;
        }
    }
}