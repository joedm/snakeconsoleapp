﻿namespace Snake.GameObjects.Blocks
{
    public class WallBlock : BaseBlock
    {
        public WallBlock(CoOrdinate coOrdinate) : base(coOrdinate, "██", Colour.Gray, Colour.Black)
        {
        }
    }
}