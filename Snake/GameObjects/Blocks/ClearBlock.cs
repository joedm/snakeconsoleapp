﻿namespace Snake.GameObjects.Blocks
{
    public class ClearBlock : BaseBlock
    {
        public ClearBlock(CoOrdinate coOrdinate) : base(coOrdinate, "  ", Colour.Black, Colour.Black)
        {
        }
    }
}