﻿namespace Snake.GameObjects.Blocks
{
    public class FoodBlock : BaseBlock
    {
        public FoodBlock(CoOrdinate coOrdinate) : base(coOrdinate, "██", Colour.Red, Colour.Black)
        {
        }
    }
}