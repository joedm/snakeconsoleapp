﻿namespace Snake.GameObjects.Blocks
{
    public class SnakeBlock : BaseBlock
    {
        public SnakeBlock(CoOrdinate coOrdinate) : base(coOrdinate, "██", Colour.Green, Colour.Black)
        {
        }
    }
}