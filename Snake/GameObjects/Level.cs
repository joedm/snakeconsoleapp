﻿using System;
using System.Linq;
using System.Text;
using Snake.GameObjects.Blocks;

namespace Snake.GameObjects
{
    public class Level
    {
        private const int DefaultSize = 40;
        public int Height { get; private set; }
        public int Width { get; private set; }
        public BaseBlock[][] Grid { get; set; }
        public Snake Snake { get; set; }
        public int FoodCollected { get; set; }
        public int PointsPerFood { get; set; } = 10;
        public bool GameOver { get; set; } = false;
        public int Moves { get; set; }
        public int Score => PointsPerFood * FoodCollected;
        public int TotalGridArea => Height * Width;

        public Level(int height, int width, int snakeLength)
        {
            Height = height;
            Width = width;

            Grid = new BaseBlock[Height][];
            for (var index = 0; index < Grid.Length; index++)
            {
                Grid[index] = new BaseBlock[Width];
            }

            Snake = new Snake(Width / 2, Height / 2, snakeLength);

            InitializeWalls();
            DrawSnake();
            SpawnFoodBlock();

            FoodCollected = 0;
        }

        public string DebuggerGridString()
        {
            var sb = new StringBuilder();
            foreach (var row in Grid)
            {
                sb.AppendLine(string.Join("", row.Select(x => x?.Characters ?? "  ")));
            }

            return sb.ToString();
        }

        public void Move()
        {
            var oldTail = Snake.CurrentTail;
            Snake.Move();

            var blockAtSnakeHead = Grid[Snake.CurrentHead.X][Snake.CurrentHead.Y];
            if (blockAtSnakeHead is WallBlock || blockAtSnakeHead is SnakeBlock)
            {
                GameOver = true;
                return;
            }

            if (blockAtSnakeHead is FoodBlock)
            {
                FoodCollected++;
                Snake.Blocks.Add(oldTail);
                SpawnFoodBlock();
            }
            else
            {
                if (oldTail.X > 0) // custom snakes can go out of bounds.
                    Grid[oldTail.X][oldTail.Y] = new ClearBlock(oldTail);
            }

            DrawSnake();
            Moves++;
        }

        private void SpawnFoodBlock()
        {
            var eligibleBlocks = Grid.SelectMany(x => x.Select(y => y)).Where(x => x is ClearBlock);
            var randomBlock = eligibleBlocks.OrderBy(x => Guid.NewGuid()).First();
            Grid[randomBlock.CoOrdinate.X][randomBlock.CoOrdinate.Y] = new FoodBlock(randomBlock.CoOrdinate);
        }
        
        private void InitializeWalls()
        {
            for (int xIndex = 0; xIndex < Grid.Length; xIndex++)
            {
                BaseBlock[] xRow = Grid[xIndex];
                for (int yIndex = 0; yIndex < xRow.Length; yIndex++)
                {
                    var coOrd = new CoOrdinate(xIndex, yIndex);

                    if (xIndex == 0)
                    {
                        xRow[yIndex] = new WallBlock(coOrd);
                        continue;
                    }

                    if (yIndex == 0 || yIndex == xRow.Length - 1)
                    {
                        xRow[yIndex] = new WallBlock(coOrd);
                        continue;
                    }

                    if (xIndex == Grid.Length - 1)
                    {
                        xRow[yIndex] = new WallBlock(coOrd);
                        continue;
                    }

                    xRow[yIndex] = new ClearBlock(coOrd);
                }
            }
        }

        private void DrawSnake()
        {
            foreach (CoOrdinate coOrd in Snake.Blocks)
            {
                // When spawning a custom snake it starts out of bounds.
                if (coOrd.X > 0)
                    Grid[coOrd.X][coOrd.Y] = new SnakeBlock(coOrd);
            }
        }
    }
}