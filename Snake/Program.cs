﻿using System;
using Snake.GameObjects;

namespace Snake
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Snake, A windows console application!");
            Console.WriteLine("Press 'Enter' to continue with default settings");
            string input = Console.ReadLine();

            var gm = new GameManager();

            if (string.IsNullOrWhiteSpace(input))
            {
                gm.Start();
            }
            else
            {
                Console.WriteLine("Grid Height: (40)");
                if (int.TryParse(Console.ReadLine(), out int height))
                    height = Math.Max(height, 5);
                else
                    height = 40;

                Console.WriteLine("Grid Width: (40)");
                if (int.TryParse(Console.ReadLine(), out int width))
                    width = Math.Max(width, 5);
                else
                    width = 40;
                
                Console.WriteLine("Snake Starting Length: (5)");
                if (int.TryParse(Console.ReadLine(), out int snakeLength))
                    snakeLength = Math.Max(snakeLength, 3);
                else
                    snakeLength = 5;

                Console.WriteLine("Starting Score: (0)");
                if (!int.TryParse(Console.ReadLine(), out int score))
                    score = int.MinValue;

                Console.WriteLine("Starting Speed: (1-10)");
                if (int.TryParse(Console.ReadLine(), out int speed))
                    speed = Math.Max(Math.Min(speed, 10), 1);
                else
                    speed = 5;

                Console.WriteLine("Difficulty: (1-10)");
                if (int.TryParse(Console.ReadLine(), out int difficulty))
                    difficulty = Math.Max(Math.Min(difficulty, 10), 1);
                else
                    difficulty = 4;

                gm.Start(height, width, snakeLength, score, difficulty, speed);
            }
        }
    }
}
